public class Factorial{
    
    public static int factorialI(int x){
        int factor = 1;
        int counter = 1;
        
        while(counter<x){
            factor = factor*++counter;
        }
        return factor;
    }
//========================================================
    public static int factorial(int i){
        if(i == 0){
            return 1;
        }
        return i * factorial(i-1);
    }
//========================================================
    public static void main(String[] args){
        System.out.println("Hello BU");
        System.out.println(factorial(4));
        System.out.println(factorialI(4));
    }
}